# Friendly quote API with DRF
done for Python Pizza Hamburg 2021 
https://youtu.be/hXKdtY8gHbs
https://hamburg.python.pizza/

### how to run the project

Clone the repository:
```
git clone https://gitlab.com/orukshyna/friendly-quote-api-with-drf.git
```

Create venv and activate it:
```
pipenv install django==2.2.6
pipenv shell
```

Setup requirements:
```
pip install -r requirements.txt
```

Perform migrations from quote_api_project:
```
cd quote_api_project
python manage.py makemigrations
python manage.py migrate
```

Run the project:
```
python manage.py runserver
```

Open endpoint in browser:
```
http://127.0.0.1:8000/api/friendly-quotes/
```

POST your friendly quote 😉
