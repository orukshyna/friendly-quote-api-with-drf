from django.db import models


class Quote(models.Model):
    phrase = models.CharField(max_length=400)
    author = models.CharField(max_length=30)

    def __str__(self):
        return self.phrase
